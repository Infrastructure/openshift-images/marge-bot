FROM registry.access.redhat.com/ubi8/python-312

ARG POETRY_VERSION=1.8.4
RUN pip install poetry==$POETRY_VERSION

ARG MARGE_COMMIT="da8bb37b83363c2f013132e1973726f942bbf4a5"
RUN git clone https://gitlab.com/marge-org/marge-bot.git marge-bot && \
    cd marge-bot && \
    git checkout $MARGE_COMMIT && \
    poetry export -o requirements.txt && \
    poetry build && \
    pip install --no-deps -r requirements.txt && \
    pip install dist/marge_bot*.tar.gz

ENTRYPOINT ["/opt/app-root/bin/marge"]
